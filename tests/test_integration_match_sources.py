# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration tests for source file matching"""
from tests.test_integration import BEAKER_XML_J2
from tests.test_integration import INDEX_BASE_YAML
from tests.test_integration import IntegrationTests
from tests.test_integration import assets_mkdir
from tests.test_integration import get_patch_path
from tests.test_integration import kpet_run
from tests.test_integration import kpet_run_generate
from tests.test_integration import kpet_run_test_list


class IntegrationMatchSourcesTests(IntegrationTests):
    """Integration tests for source file matching"""

    def test_one_case_no_sources(self):
        """Test source-matching a case with no sources"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case1:
                      name: case1
                      max_duration_seconds: 600
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Matches baseline
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Matches patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')

    def test_one_case_one_pattern(self):
        """Test source-matching a case with one pattern"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      target_sources:
                        - a
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Matches baseline
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Matches patches it should
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Doesn't match patches it shouldn't
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*</job>.*')

    def test_one_case_two_patterns(self):
        """Test source-matching a case with two patterns"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      target_sources:
                        - a
                        - d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Matches baseline
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Matches first patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Matches second patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Matches both patches only once
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Doesn't match patches it shouldn't
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_ghi.diff"),
                stdout_matching=r'.*<job>\s*</job>.*')

    def test_two_cases(self):
        """Test source-matching two cases"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      target_sources:
                        - a
                    case2:
                      name: case2
                      max_duration_seconds: 600
                      target_sources:
                        - d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Both match baseline
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*'
                                r'case2\s*</job>.*')
            # First matches its patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Second matches its patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case2\s*</job>.*')
            # Both match their patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*'
                                r'case2\s*</job>.*')
            # None match other patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_ghi.diff"),
                stdout_matching=r'.*<job>\s*</job>.*')

    def test_two_suites(self):
        """Test source-matching two suites"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      target_sources:
                        - a
                    case2:
                      name: case2
                      max_duration_seconds: 600
                      target_sources:
                        - d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Both match baseline
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*'
                                r'case2\s*</job>.*')
            # First matches its patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Second matches its patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case2\s*</job>.*')
            # Both match their patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*'
                                r'case2\s*</job>.*')
            # None match other patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_ghi.diff"),
                stdout_matching=r'.*<job>\s*</job>.*')

    def test_high_cost_parent(self):
        """Test source-matching with a high-cost parent case"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    A:
                      name: A
                      high_cost: true
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
                          target_sources: a
                    B:
                      name: B
                      cases:
                        b:
                          name: b
                          max_duration_seconds: 600
                          target_sources: d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Only "normal-cost" case matches baseline
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*B - b\s*</job>.*')
            # First matches its patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*A - a\s*</job>.*')
            # Second matches its patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*B - b\s*</job>.*')
            # All cases can match if provided appropriate patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*A - a\s*'
                                r'B - b\s*</job>.*')
            # None match other patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_ghi.diff"),
                stdout_matching=r'.*<job>\s*</job>.*')

    def test_high_cost_leaf(self):
        """Test source-matching with a high-cost leaf case"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    A:
                      name: A
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
                          high_cost: true
                          target_sources: a
                    B:
                      name: B
                      cases:
                        b:
                          name: b
                          max_duration_seconds: 600
                          target_sources: d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Only "normal-cost" case matches baseline
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*B - b\s*</job>.*')
            # First matches its patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*A - a\s*</job>.*')
            # Second matches its patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*B - b\s*</job>.*')
            # All cases can match if provided appropriate patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*A - a\s*'
                                r'B - b\s*</job>.*')
            # None match other patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_ghi.diff"),
                stdout_matching=r'.*<job>\s*</job>.*')

    def test_high_cost_inheritance(self):
        """Test inheritance of high-cost property"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    HIGH-COST-PARENT:
                      high_cost: true
                      cases:
                        high-cost-parent:
                          name: high-cost-parent
                          target_sources: a
                          max_duration_seconds: 600
                    HIGH-COST-LEAF:
                      cases:
                        high-cost-leaf:
                          name: high-cost-leaf
                          high_cost: true
                          target_sources: a
                          max_duration_seconds: 600
                    HIGH-COST-OVERRIDE:
                      high_cost: false
                      cases:
                        high-cost-override:
                          name: high-cost-override
                          target_sources: a
                          max_duration_seconds: 600
                          high_cost: true
                    NORMAL-COST:
                      cases:
                        normal-cost:
                          name: normal-cost
                          target_sources: a
                          max_duration_seconds: 600
                    NORMAL-COST-PARENT:
                      high_cost: false
                      cases:
                        normal-cost-parent:
                          name: normal-cost-parent
                          target_sources: a
                          max_duration_seconds: 600
                    NORMAL-COST-LEAF:
                      cases:
                        normal-cost-leaf:
                          name: normal-cost-leaf
                          high_cost: false
                          target_sources: a
                          max_duration_seconds: 600
                    NORMAL-COST-OVERRIDE:
                      high_cost: true
                      cases:
                        normal-cost-override:
                          name: normal-cost-override
                          target_sources: a
                          max_duration_seconds: 600
                          high_cost: false
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Only "normal-cost" cases match baseline
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*'
                r'normal-cost\s*'
                r'normal-cost-parent\s*'
                r'normal-cost-leaf\s*'
                r'normal-cost-override\s*'
                r'</job>.*')
            # All cases match with their patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*'
                r'high-cost-parent\s*'
                r'high-cost-leaf\s*'
                r'high-cost-override\s*'
                r'normal-cost\s*'
                r'normal-cost-parent\s*'
                r'normal-cost-leaf\s*'
                r'normal-cost-override\s*'
                r'</job>.*')
            # None match other patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_ghi.diff"),
                stdout_matching=r'.*<job>\s*</job>.*')

    def test_target_sources_inheritance(self):
        """Test inheritance of target_sources property"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    NONE:
                      cases:
                        none:
                          name: none
                          max_duration_seconds: 600
                    PARENT:
                      target_sources: a
                      cases:
                        parent:
                          name: parent
                          max_duration_seconds: 600
                    LEAF:
                      cases:
                        leaf:
                          name: leaf
                          target_sources: a
                          max_duration_seconds: 600
                    BOTH:
                      target_sources: a
                      cases:
                        both:
                          name: both
                          target_sources: d
                          max_duration_seconds: 600
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # All cases match baseline
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*'
                r'none\s*'
                r'parent\s*'
                r'leaf\s*'
                r'both\s*'
                r'</job>.*')
            # All cases match first patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*'
                r'none\s*'
                r'parent\s*'
                r'leaf\s*'
                r'both\s*'
                r'</job>.*')
            # Only "untargeted" and merged cases match second patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*'
                r'none\s*'
                r'both\s*'
                r'</job>.*')
            # Only "untargeted" matches other patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_ghi.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*'
                r'none\s*'
                r'</job>.*')

    def test_command_line_combinations(self):
        """
        Test various combinations of ways to specify changed files on the
        command line.
        """
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case_a:
                      name: case_a
                      max_duration_seconds: 600
                      target_sources:
                        - a
                    case_d:
                      name: case_d
                      max_duration_seconds: 600
                      target_sources:
                        - d
                    case_g:
                      name: case_g
                      max_duration_seconds: 600
                      target_sources:
                        - g
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # None specified (unknown changed files)
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*case_g\s*'
                                r'</job>.*')
            # Specified with one MBOX positional argument
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*'
                                r'</job>.*')
            # Specified with two MBOX positional arguments
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*'
                                r'</job>.*')
            # Specified with one -f/--file option
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "-fa",
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*'
                                r'</job>.*')
            # Specified with two -f/--file options
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "-fa", "-fd",
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*'
                                r'</job>.*')
            # Specified with one MBOX positional argument and one -f option
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                "-fd",
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*'
                                r'</job>.*')
            # Specified with one -m/--mboxes option containing one mbox
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--mboxes",
                get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*'
                                r'</job>.*')
            # Specified with one -m/--mboxes option containing two mboxes
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--mboxes",
                get_patch_path("misc/files_abc.diff") + " " +
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*'
                                r'</job>.*')
            # Two -m/--mboxes options specifying different mboxes
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--mboxes",
                get_patch_path("misc/files_abc.diff"),
                "--mboxes",
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_d\s*'
                                r'</job>.*')
            # One -m/--mboxes options specifying "unknown files"
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--mboxes", "",
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*case_g\s*'
                                r'</job>.*')
            # One -m/--mboxes options specifying "no files"
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--mboxes", " ",
                stdout_matching=r'.*<job>\s*'
                                r'</job>.*')
            # Specified with one MBOX positional argument and one -m option
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                "-m",
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*'
                                r'</job>.*')
            # Specified with one -f option and one -m option
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "-fa",
                "-m", get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*'
                                r'</job>.*')
            # Specified using all the ways
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                "-fd",
                "-m", get_patch_path("misc/files_ghi.diff"),
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*case_g\s*'
                                r'</job>.*')
            # Specifying unknown with just the -m option
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                "-fd",
                "-m", "",
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*'
                                r'</job>.*')
            # Specifying no files with just the -m option
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                "-fd",
                "-m", " ",
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*'
                                r'</job>.*')
            # Two executions with none specified (unknown changed files)
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--execute",
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*case_g\s*'
                                r'HOST\s*case_a\s*case_d\s*case_g\s*'
                                r'</job>.*')
            # Two executions with second explicitly specifying unknown
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--execute", "-m", "",
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*case_g\s*'
                                r'HOST\s*case_a\s*case_d\s*case_g\s*'
                                r'</job>.*')
            # Two executions with different mailboxes specified
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "-m", get_patch_path("misc/files_def.diff"),
                "--execute",
                "-m", get_patch_path("misc/files_ghi.diff"),
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_d\s*'
                                r'HOST\s*case_g\s*'
                                r'</job>.*')
            # Two executions with extra different mailboxes specified
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "-fa",
                "-m", get_patch_path("misc/files_def.diff"),
                "--execute",
                "-m", get_patch_path("misc/files_ghi.diff"),
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*'
                                r'HOST\s*case_a\s*case_g\s*'
                                r'</job>.*')
            # Two executions with implicit unknown first
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--execute",
                "-m", get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*case_g\s*'
                                r'HOST\s*case_a\s*'
                                r'</job>.*')
            # Two executions with explicit unknown first
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "-m", "",
                "--execute",
                "-m", get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*case_g\s*'
                                r'HOST\s*case_a\s*'
                                r'</job>.*')
            # Two executions with no files first
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "-m", " ",
                "--execute",
                "-m", get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*'
                                r'</job>.*')
            # Two executions with only first specified
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "-m", get_patch_path("misc/files_abc.diff"),
                "--execute",
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*'
                                r'HOST\s*case_a\s*'
                                r'</job>.*')
            # Two executions with only first extra specified
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                "-m", get_patch_path("misc/files_def.diff"),
                "--execute",
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*'
                                r'HOST\s*case_a\s*case_d\s*'
                                r'</job>.*')
            # Two executions with second specifying unknown files
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "-m", get_patch_path("misc/files_abc.diff"),
                "--execute",
                "-m", "",
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*'
                                r'HOST\s*case_a\s*case_d\s*case_g\s*'
                                r'</job>.*')
            # Two executions with extras and second specifying unknown files
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                "-m", get_patch_path("misc/files_def.diff"),
                "--execute",
                "-m", "",
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*'
                                r'HOST\s*case_a\s*'
                                r'</job>.*')
            # Two executions with extras and second specifying no files
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                "-m", get_patch_path("misc/files_def.diff"),
                "--execute",
                "-m", " ",
                stdout_matching=r'.*<job>\s*'
                                r'HOST\s*case_a\s*case_d\s*'
                                r'HOST\s*case_a\s*'
                                r'</job>.*')

    def test_triggered_targeted_options(self):
        """
        Test --triggered/--targeted options work correctly for all "kpet run"
        subcommands.
        """
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                  universal_id: test_id
                  host_types: ^normal
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  max_duration_seconds: 600
                  cases:
                    extra:
                      name: extra
                    targeted:
                      name: targeted
                      target_sources: target
                    triggered:
                      name: triggered
                      trigger_sources: trigger
            """,
            "output.txt.j2": """
                {%- for scene in SCENES -%}
                  {%- for recipeset in scene.recipesets -%}
                    {%- for HOST in recipeset -%}
                      {%- for test in HOST.tests -%}
                        {{- test.name + "\\n" -}}
                      {%- endfor -%}
                    {%- endfor -%}
                  {%- endfor -%}
                {%- endfor -%}
            """,
        }

        with assets_mkdir(assets) as db_path:
            for extra_argv, stdout in {
                ("-ftrigger", "-ftarget", "-fother",):
                    "other\ntarget\ntrigger\n",
                ("-ftrigger", "-ftarget", "-fother",
                 "--triggered",):
                    "other\ntarget\ntrigger\n",
                ("-ftrigger", "-ftarget", "-fother",
                 "--targeted",):
                    "other\ntarget\ntrigger\n",

                ("-ftrigger", "-ftarget", "-fother",
                 "--triggered-sources",):
                    "target\ntrigger\n",
                ("-ftrigger", "-ftarget", "-fother",
                 "--triggered-sources", "--triggered",):
                    "target\ntrigger\n",
                ("-ftrigger", "-ftarget", "-fother",
                 "--triggered-sources", "--targeted",):
                    "target\n",

                ("-ftrigger", "-ftarget", "-fother",
                 "--targeted-sources",):
                    "target\n",
                ("-ftrigger", "-ftarget", "-fother",
                 "--targeted-sources", "--triggered",):
                    "target\n",
                ("-ftrigger", "-ftarget", "-fother",
                 "--targeted-sources", "--targeted",):
                    "target\n",
            }.items():
                self.assertKpetProduces(
                    kpet_run, db_path, ["source", "list"],
                    *extra_argv, stdout_equals=stdout
                )

            for argv in ([kpet_run_generate, db_path, "--no-lint"],
                         [kpet_run_test_list, db_path]):
                for extra_argv, stdout in {
                    tuple():
                        "extra\ntargeted\ntriggered\n",
                    ("--triggered",):
                        "",
                    ("--triggered=no",):
                        "extra\ntargeted\ntriggered\n",
                    ("--targeted",):
                        "",
                    ("--targeted=no",):
                        "extra\ntargeted\ntriggered\n",
                    ("-ftrigger", "-ftarget",):
                        "extra\ntargeted\ntriggered\n",
                    ("-fother",):
                        "extra\n",
                    ("-ftrigger", "-ftarget",
                     "--triggered",):
                        "targeted\ntriggered\n",
                    ("-ftrigger", "-ftarget",
                     "--triggered=no",):
                        "extra\n",
                    ("-ftrigger", "-ftarget",
                     "--targeted",):
                        "targeted\n",
                    ("-ftrigger", "-ftarget",
                     "--targeted=no",):
                        "extra\ntriggered\n",
                }.items():
                    self.assertKpetProduces(
                        *argv, *extra_argv, stdout_equals=stdout
                    )
